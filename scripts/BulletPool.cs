using System.Collections.Generic;
using Godot;

public partial class BulletPool: Node3D
{
    private static string bulletScenePath = "res://nodes/bullet.tscn";
    private List<Bullet> activePool;
    private List<Bullet> freePool;
    private int initialPoolSize = 30;

    public override void _Ready()
	{
        EnsureInitialised();
	}

    public void FireBullet(Vector3 position, Vector3 direction, float speed = 10.0f, float timeToLive = 2.0f)
    {
        if (freePool.Count == 0)
        {
            freePool.Add(CreateNewBullet());
        }
        int bulletIndex = freePool.Count - 1;
        Bullet bullet = freePool[bulletIndex];
        freePool.RemoveAt(bulletIndex);
        GetTree().Root.AddChild(bullet);
        bullet.FireBullet(position, direction, speed, timeToLive, ReturnToPool);
        activePool.Add(bullet);
        GD.Print(bullet);
    }

    private void EnsureInitialised()
    {
        if (freePool == null)
        {
            activePool = new List<Bullet>();
            freePool = new List<Bullet>();
            for (int i = 0; i < initialPoolSize; i++)
            {
                freePool.Add(CreateNewBullet());
            }
        }
    }

    private Bullet CreateNewBullet()
    {
        Bullet bullet = GD.Load<PackedScene>(bulletScenePath).Instantiate<Bullet>();
        return bullet;
    }

    private void ReturnToPool(Bullet bullet)
    {
        this.activePool.Remove(bullet);
        this.freePool.Add(bullet);
    }
}
