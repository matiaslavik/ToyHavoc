using Godot;
using System;

public partial class Vehicle : StaticBody3D, IDamageable
{
	[Export]
	public Node3D explosionSocket;

	[Export]
	public AnimationPlayer animationPlayer;

	[Export]
	public AudioStreamPlayer3D explosionStreamPlayer;

	public override void _Ready()
	{
	}

	public override void _Process(double delta)
	{
	}

    public void TakeDamage(float damage)
    {
        Destroy();
    }

	private void Destroy()
	{
		GD.Print("DESTROY");
        Node3D explosionNode = GD.Load<PackedScene>("res://effects/explosion1.tscn").Instantiate<Node3D>();
		Node3D socket = explosionSocket != null ? explosionSocket : this;
		socket.AddChild(explosionNode);
		explosionNode.Position = Vector3.Zero;

		if (animationPlayer != null)
		{
			animationPlayer.Play("explode");
		}

		if (explosionStreamPlayer != null)
		{
			explosionStreamPlayer.Play();
		}
	}
}
