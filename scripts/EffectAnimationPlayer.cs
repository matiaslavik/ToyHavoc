using Godot;
using System;

public partial class EffectAnimationPlayer : AnimationPlayer
{
	[Export]
	public string autoPlayAnimationName;

	public override void _Ready()
	{
		if (!String.IsNullOrEmpty(autoPlayAnimationName))
		{
			this.Play(autoPlayAnimationName);
		}
	}
}
