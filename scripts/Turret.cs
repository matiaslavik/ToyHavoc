using Godot;
using System;

public partial class Turret : Node3D
{
	[Export]
	public AudioStreamPlayer3D gunStreamPlayer;

	private float cooldown = 0.0f;
	private float cooldownDelay = 0.1f;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (CanFire() && Input.IsKeyPressed(Key.Space))
		{
			Fire();
		}
		cooldown = Mathf.Max(cooldown - (float)delta, 0.0f);
	}

	public bool CanFire()
	{
		return cooldown == 0.0f;
	}

	public void Fire()
	{
		PhysicsDirectSpaceState3D spaceState = GetWorld3D().DirectSpaceState;
		PhysicsRayQueryParameters3D query = PhysicsRayQueryParameters3D.Create(this.GlobalPosition, this.GlobalPosition - this.GlobalTransform.Basis.Z * 100.0f);
		var results = spaceState.IntersectRay(query);
		if (results.Count > 0)
		{
			GodotObject target = results["collider"].AsGodotObject();
			GD.Print(target.ToString());
			IDamageable damageable = target as IDamageable;
			if (damageable != null)
			{
				DamageSystem.PerformDamage(damageable, 10);
			}
		}
		gunStreamPlayer.Play();
		GetNode<BulletPool>("/root/BulletPool").FireBullet(this.GlobalPosition, -this.GlobalTransform.Basis.Z);
		cooldown = cooldownDelay;
	}
}
