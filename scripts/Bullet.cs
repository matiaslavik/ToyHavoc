using Godot;
using System;

public partial class Bullet : CsgSphere3D
{
	private Vector3 direction;
	private float speed;
	private float timeToLive;
	private Action<Bullet> returnToPoolCallback;

	public override void _Ready()
	{
	}

	public override void _Process(double delta)
	{
		this.GlobalPosition += this.direction * (float)delta * speed;
		timeToLive -= (float)delta;
		if (timeToLive <= 0.0f)
		{
			returnToPoolCallback.Invoke(this);
		}
	}

	public void FireBullet(Vector3 position, Vector3 direction, float speed, float timeToLive, Action<Bullet> returnToPoolCallback)
	{
		this.GlobalPosition = position;
		this.direction = direction;
		this.speed = speed;
		this.timeToLive = timeToLive;
		this.returnToPoolCallback = returnToPoolCallback;
	}
}
