public class DamageSystem
{
    public static void PerformDamage(IDamageable target, float damage)
    {
        target.TakeDamage(damage);
    }
}
